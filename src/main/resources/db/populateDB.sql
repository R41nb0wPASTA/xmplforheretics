INSERT INTO game_type(name)
VALUES ('rpg'),
       ('racing'),
       ('fighting');

INSERT INTO game(name, type_id)
VALUES ('MK_XL', '3'),
       ('StreetFighter5', '3'),
       ('DarkSouls', '1'),
       ('FlatOut', '2');

INSERT INTO users(name, password)
VALUES ('420', 'legit');